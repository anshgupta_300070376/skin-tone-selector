package com.example.virtuallipstickmlkit;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.example.virtuallipstickmlkit.adapter.ColorAdapter;
import com.example.virtuallipstickmlkit.adapter.RecyclerViewClickListener;
import com.example.virtuallipstickmlkit.utils.DrawUtils;
import com.example.virtuallipstickmlkit.utils.MakeupBeautyUtils;
import com.example.virtuallipstickmlkit.utils.MyPorterDuffMode;
import com.example.virtuallipstickmlkit.utils.util;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private ImageView imageView;
    private MakeupBeautyUtils makeupBeautyUtils;
    private FirebaseVisionFaceDetector detector;
    private List<FirebaseVisionFace> firebaseFaces;
    private RecyclerView colorSelector;
    private List<String> colors = new ArrayList<>();
    private int selectedColorIndex = 0;
    private Bitmap imgBitmap;
    private int approach = 2;
    private BlendMode bMode = BlendMode.CLEAR;
    private int lipAlpha = 160;
    private int skinTone = 0;
    Bitmap lipsMask;
    Bitmap lipMaskFromUrl;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if(msg.obj instanceof Bitmap){
                if(msg.what == 0) {
                    drawLipstick((Bitmap) msg.obj);
                }else if(msg.what == 1){
                    imageView.setImageBitmap((Bitmap) msg.obj);
                }
            }
            return false;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Firebase MLKit initialization
        FirebaseVisionFaceDetectorOptions options = new FirebaseVisionFaceDetectorOptions.Builder()
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
                .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
                .enableTracking()
                .build();

        detector = FirebaseVision.getInstance()
                .getVisionFaceDetector(options);

        makeupBeautyUtils = new MakeupBeautyUtils();
        imageView = findViewById(R.id.img);
        CheckBox checkBox = findViewById(R.id.checkBox);
        Spinner spinner = findViewById(R.id.spinner);
        SeekBar seekBar = findViewById(R.id.seekBar);
        TextView alphaText = findViewById(R.id.alphaText);
        Button nextButton = findViewById(R.id.nextButton);
        Button button = findViewById(R.id.button);
        colorSelector = findViewById(R.id.colour_selector);

        if(approach == 3) button.setVisibility(View.VISIBLE);

        addColors();
        setColorsAdapter();
        if(approach != 3){
            load();
        }

        String[] blendingModes = {"NONE", "CLEAR", "SRC", "DST", "SRC_OVER", "DST_OVER", "SRC_IN", "DST_IN", "SRC_OUT", "DST_OUT", "SRC_ATOP",
                "DST_ATOP", "XOR", "PLUS", "MODULATE", "SCREEN", "OVERLAY", "DARKEN", "LIGHTEN",
                "COLOR_DODGE", "COLOR_BURN", "HARD_LIGHT", "SOFT_LIGHT", "DIFFERENCE","EXCLUSION", "MULTIPLY",
                "HUE", "SATURATION", "COLOR", "LUMINOSITY"
        };

        ArrayAdapter<String>adapter = new ArrayAdapter<>(MainActivity.this,
                android.R.layout.simple_spinner_item,blendingModes);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                skinTone = (skinTone + 1) % 6;
                load();
            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                lipAlpha = i;
                makeupBeautyUtils.progress(imgBitmap,handler);
                alphaText.setText("Alpha : " + i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                approach = isChecked ? 2 : 1;
                load();
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                    } else {
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(galleryIntent, 2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void addColors() {
        colors.add("#ff339a");
        colors.add("#a7192f");
        colors.add("#df477a");
        colors.add("#ff339a");
        colors.add("#e21d79");
        colors.add("#e01a58");
        colors.add("#dd0747");
        colors.add("#ff4e7a");
        colors.add("#f17da4");
        colors.add("#d24259");
        colors.add("#f0a6b3");
        colors.add("#ff73df");
        colors.add("#d290e7");
        colors.add("#ffaeff");
        colors.add("#791555");
        colors.add("#8b1255");

//        colors.add("#a7192f");
//        colors.add("#ba1b4b");
//        colors.add("#8b2648");
//        colors.add("#7e3a3b");
//        colors.add("#bf464d");
//        colors.add("#a0355f");
//        colors.add("#c45156");
//        colors.add("#e1392e");
//        colors.add("#8143dc");
    }

    private void setColorsAdapter() {

        RecyclerViewClickListener listener = (position) -> {
            selectedColorIndex = position;
            lipsMask = getLipsMask();
            makeupBeautyUtils.progress(imgBitmap,handler);
        };

        ColorAdapter colorsAdapter = new ColorAdapter(colors, getApplicationContext(), listener);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        colorSelector.setLayoutManager(layout);
        colorSelector.setItemAnimator( new DefaultItemAnimator());
        colorSelector.setAdapter(colorsAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 2);
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && data != null && data.getExtras()!=null) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(image);
        } else if (requestCode == 2 && data != null) {
            Uri selectedImage = data.getData();
            if(selectedImage == null) return;
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            if(cursor == null) return;
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            try {
                imgBitmap = modifyOrientation(BitmapFactory.decodeFile(picturePath), picturePath);
                imageView.setImageBitmap(imgBitmap);
                BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                load(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private void load() {
//        switch (skinTone) {
//            case 0:
//                imageView.setImageResource(R.drawable.skin_tone1);
//                break;
//            case 1:
//                imageView.setImageResource(R.drawable.skin_tone2);
//                break;
//            case 2:
//                imageView.setImageResource(R.drawable.skin_tone3);
//                break;
//            case 3:
//                imageView.setImageResource(R.drawable.skin_tone4);
//                break;
//            case 4:
//                imageView.setImageResource(R.drawable.skin_tone5);
//                break;
//            case 5:
//                imageView.setImageResource(R.drawable.skin_tone6);
//                break;
//        }
//        BitmapDrawable drawable = (BitmapDrawable) imageView.getDrawable();
//        imgBitmap = drawable.getBitmap();
//        makeupBeautyUtils.progress(imgBitmap,handler);

        Glide.with(this)
                .asBitmap()
                .load("https://assets.myntassets.com/assets/images/retaillabs/2020/11/10/7b6d6630-52e9-4a22-9388-26c162d26be71605007792990-skin_tone1.jpg")
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        imgBitmap = resource;
                        if(imgBitmap != null && lipMaskFromUrl != null) drawFinalImage();
                    }
                });

        Glide.with(this)
                .asBitmap()
                .load("https://assets.myntassets.com/assets/images/retaillabs/2020/11/11/379deaac-f2ed-402c-aefe-3eda272f4ba11605080652430-lips_grayscale_white.png")
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                        lipMaskFromUrl = resource;
                        if(imgBitmap != null && lipMaskFromUrl != null) drawFinalImage();
                    }
                });

    }

    private void drawFinalImage() {
        makeupBeautyUtils.progress(imgBitmap,handler);
        lipsMask = getLipsMask();
    }

    // Firebase MLKit load function
    private void load(final Bitmap bitmap) {
        try {
            FirebaseVisionImage firebaseImage = FirebaseVisionImage.fromBitmap(bitmap);

            detector.detectInImage(firebaseImage)
                    .addOnSuccessListener(
                            new OnSuccessListener<List<FirebaseVisionFace>>() {
                                @Override
                                public void onSuccess(List<FirebaseVisionFace> faces) {
                                    makeupBeautyUtils.progress(bitmap,handler);
                                    firebaseFaces = faces;
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                }
                            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap getLipsMask() {
        String colorHexCode = colors.get(selectedColorIndex);
        return changeBitmapColor(getBitmap(imgBitmap.getWidth(), imgBitmap.getHeight()), colorHexCode);
    }

    private Bitmap changeBitmapColor(Bitmap bitmap, String hexCode) {
        Paint paint = new Paint();
        Canvas canvas = new Canvas(bitmap);

        int color = Color.parseColor(hexCode);
        LightingColorFilter filter = new LightingColorFilter(0, color);

        paint.setColorFilter(filter);
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return bitmap;
    }

    private Bitmap getBitmap(int width, int height) {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inMutable = true;
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.lips_grayscale_white, options);

        return Bitmap.createScaledBitmap(lipMaskFromUrl, width, height, false);
    }

    private int getAlpha(int alpha) {
        if (alpha > 80) {
            alpha = (int) (alpha * 0.9f + 0.5f);
        }
        return alpha;
    }

    private void drawLipstick(Bitmap bitmap){
        DrawUtils drawUtils = new DrawUtils();

        // for firebase MLKit face detection model
        // drawUtils.draw(bitmap,firebaseFaces);

        int alpha = getAlpha(getAlpha(lipAlpha));

        switch (approach){
            case 1:
                drawUtils.draw(this, bitmap, colors.get(selectedColorIndex), "lips_points.json", bMode, alpha);
                break;
            case 2:
                Bitmap blendedLipMask = blendImages(lipsMask, bitmap);
                drawUtils.draw(bitmap, util.blur(getApplicationContext(), blendedLipMask, 3f), alpha, bMode);
                //drawUtils.draw(bitmap, lipsMask, alpha, bMode);
                break;
            case 3:
                drawUtils.draw(bitmap, colors.get(selectedColorIndex), firebaseFaces, alpha, bMode);
                break;
        }

        imageView.setImageBitmap(bitmap);
    }

    private Bitmap blendImages(Bitmap src, Bitmap dst) {
        MyPorterDuffMode myPorterDuffMode = new MyPorterDuffMode();
        Matrix matrix = new Matrix();
        Bitmap srcBitmap = Bitmap.createBitmap(src, 0, 0, imgBitmap.getWidth(), imgBitmap.getHeight(), matrix, true);
        Bitmap dstBitmap = Bitmap.createBitmap(dst, 0, 0, imgBitmap.getWidth(), imgBitmap.getHeight(), matrix, true);

        return myPorterDuffMode.applyOverlayMode(srcBitmap, dstBitmap);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        switch (position) {
            case 0:
                bMode = null;
                break;
            case 1:
                bMode = BlendMode.CLEAR;
                break;
            case 2:
                bMode = BlendMode.SRC;
                break;
            case 3:
                bMode = BlendMode.DST;
                break;
            case 4:
                bMode = BlendMode.SRC_OVER;
                break;
            case 5:
                bMode = BlendMode.DST_OVER;
                break;
            case 6:
                bMode = BlendMode.SRC_IN;
                break;
            case 7:
                bMode = BlendMode.DST_IN;
                break;
            case 8:
                bMode = BlendMode.SRC_OUT;
                break;
            case 9:
                bMode = BlendMode.DST_OUT;
                break;
            case 10:
                bMode = BlendMode.SRC_ATOP;
                break;
            case 11:
                bMode = BlendMode.DST_ATOP;
                break;
            case 12:
                bMode = BlendMode.XOR;
                break;
            case 13:
                bMode = BlendMode.PLUS;
                break;
            case 14:
                bMode = BlendMode.MODULATE;
                break;
            case 15:
                bMode = BlendMode.SCREEN;
                break;
            case 16:
                bMode = BlendMode.OVERLAY;
                break;
            case 17:
                bMode = BlendMode.DARKEN;
                break;
            case 18:
                bMode = BlendMode.LIGHTEN;
                break;
            case 19:
                bMode = BlendMode.COLOR_DODGE;
                break;
            case 20:
                bMode = BlendMode.COLOR_BURN;
                break;
            case 21:
                bMode = BlendMode.HARD_LIGHT;
                break;
            case 22:
                bMode = BlendMode.SOFT_LIGHT;
                break;
            case 23:
                bMode = BlendMode.DIFFERENCE;
                break;
            case 24:
                bMode = BlendMode.EXCLUSION;
                break;
            case 25:
                bMode = BlendMode.MULTIPLY;
                break;
            case 26:
                bMode = BlendMode.HUE;
                break;
            case 27:
                bMode = BlendMode.SATURATION;
                break;
            case 28:
                bMode = BlendMode.COLOR;
                break;
            case 29:
                bMode = BlendMode.LUMINOSITY;
                break;
        }
        makeupBeautyUtils.progress(imgBitmap,handler);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

}
