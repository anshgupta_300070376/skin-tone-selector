package com.example.virtuallipstickmlkit.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.virtuallipstickmlkit.R;
import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ColorPill>{

    private List<String> colors;
    private Context context;
    private int selectedColorIndex = 0;
    private RecyclerViewClickListener mListener;

    public ColorAdapter(List<String> colors, Context context, RecyclerViewClickListener listener) {
        this.colors = colors;
        this.context = context;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ColorPill onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.color_pill, parent, false);
        return new ColorPill(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ColorPill colorPill, @SuppressLint("RecyclerView") final int position) {
        colorPill.color.setColorFilter(Color.parseColor(colors.get(position)));

        if(position == selectedColorIndex){
            colorPill.colorContainer.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FF3F6C")));
        }
        else
            colorPill.colorContainer.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#FFFFFF")));

        colorPill.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedColorIndex = position;
                mListener.onClick(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        int arr = 0;
        try{ arr = colors.size(); }
        catch (Exception ignored){ }
        return arr;
    }

    static class ColorPill extends RecyclerView.ViewHolder{
        ImageView color;
        ConstraintLayout colorContainer;

        ColorPill(View itemView) {
            super(itemView);
            color = itemView.findViewById(R.id.color);
            colorContainer = itemView.findViewById(R.id.color_container);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
