package com.example.virtuallipstickmlkit.adapter;

public interface RecyclerViewClickListener {
    void onClick(int position);
}
