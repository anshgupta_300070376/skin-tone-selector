package com.example.virtuallipstickmlkit.graphicDraw;

import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.Log;

public class LipDraw {

    public static int alphaColor(int color, int alpha) {
        return (color & 0x00FFFFFF) | alpha;
    }

    public static void drawLipstick(Canvas canvas, Path lipPath, String lipstickHexCode, int alpha, BlendMode mode) {
        int color = Color.parseColor(lipstickHexCode);

        alpha = (int) (Color.alpha(color) * ((float) alpha / 255)) << 24;
        color = alphaColor(color, alpha);
        float blur_radius = 3;

        RectF bounds = new RectF();
        lipPath.computeBounds(bounds, true);
        bounds.inset(-blur_radius, -blur_radius);

//        Log.e("TESTING ---> ", Float.toString(bounds.left)+ "  "+ Float.toString(bounds.top));

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        if(mode != null) paint.setBlendMode(mode);

        paint.setMaskFilter(new BlurMaskFilter(blur_radius, BlurMaskFilter.Blur.NORMAL));
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);

        canvas.drawPath(lipPath, paint);
    }

    public static void drawLipstick(Bitmap lipMask, Canvas canvas, int alpha, BlendMode mode) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //if(mode != null) paint.setBlendMode(mode);
        paint.setAlpha(alpha);
        canvas.drawBitmap(lipMask, 0, 0, paint);
    }
}
