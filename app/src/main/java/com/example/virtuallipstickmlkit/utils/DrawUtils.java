package com.example.virtuallipstickmlkit.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlendMode;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import com.example.virtuallipstickmlkit.graphicDraw.LipDraw;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;

public class DrawUtils {

    public void draw(Context context, Bitmap originBitmap, String lipstickHexCode, String jsonFileName, BlendMode mode, int alpha) {
        try {
            if(originBitmap == null) return;
            Canvas canvas = new Canvas(originBitmap);
            String lipJson = util.getJson(context,jsonFileName);
            Path mouthPath = getLipPath(lipJson);
            LipDraw.drawLipstick(canvas, mouthPath, lipstickHexCode,alpha, mode);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Lip mask draw function
    public void draw(Bitmap originBitmap, Bitmap lipsMask, int alpha, BlendMode mode) {
        Canvas canvas = new Canvas(originBitmap);
        LipDraw.drawLipstick(lipsMask, canvas, alpha, mode);
    }

    private Path getLipPath(String faceJson) {
        try {
            JSONObject jsonObject = new JSONObject(faceJson);
            JSONObject mouthJson = jsonObject.getJSONObject("face").getJSONObject("landmark").getJSONObject("lips");

            Path outPath = new Path();
            Path inPath = new Path();

            Point start = getPointByJson(mouthJson.getJSONObject("upper_lip_0"));
            outPath.moveTo(start.x,start.y);
            for(int i = 1;i < 32;i++){
                Point pointByJson = getPointByJson(mouthJson.getJSONObject("upper_lip_" + i));
                outPath.lineTo(pointByJson.x,pointByJson.y);
            }

            for(int i = 31;i > 0;i--){
                Point pointByJson = getPointByJson(mouthJson.getJSONObject("lower_lip_" + i));
                outPath.lineTo(pointByJson.x,pointByJson.y);
            }
            outPath.close();


            Point inStart = getPointByJson(mouthJson.getJSONObject("upper_lip_32"));
            inPath.moveTo(inStart.x,inStart.y);

            for(int i = 32;i < 64;i++){
                Point pointByJson = getPointByJson(mouthJson.getJSONObject("upper_lip_" + i));
                inPath.lineTo(pointByJson.x,pointByJson.y);
            }

            for(int i = 63;i >= 32;i--){
                Point pointByJson = getPointByJson(mouthJson.getJSONObject("lower_lip_" + i));
                inPath.lineTo(pointByJson.x,pointByJson.y);
            }

            outPath.op(inPath, Path.Op.DIFFERENCE);
            return  outPath;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Point getPointByJson(JSONObject jsonObject){
        return new Point(jsonObject.optInt("x"),jsonObject.optInt("y"));
    }

    // Firebase MLKit draw and lipPath functions

    public void draw(Bitmap originBitmap, String lipstickHexCode, List<FirebaseVisionFace> facePointJson,int alpha, BlendMode mode) {
        try {
            if(originBitmap == null) return;
            Canvas canvas = new Canvas(originBitmap);
            for(int face =0; face<facePointJson.size(); face++){
                Path mouthPath = getLipPath(facePointJson.get(face));
                LipDraw.drawLipstick(canvas, mouthPath, lipstickHexCode,alpha, mode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Path getLipPath(FirebaseVisionFace face){
        Path outPath = new Path();
        Path inPath = new Path();

        FirebaseVisionFaceContour upperLipTop = face.getContour(FirebaseVisionFaceContour.UPPER_LIP_TOP);
        outPath.moveTo(upperLipTop.getPoints().get(0).getX(), upperLipTop.getPoints().get(0).getY());
        for(int i=1; i<upperLipTop.getPoints().size(); i++){
            Float px = upperLipTop.getPoints().get(i).getX();
            Float py = upperLipTop.getPoints().get(i).getY();
            outPath.lineTo(px, py);
        }

        FirebaseVisionFaceContour lowerLipBottom = face.getContour(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM);
        for(int i=0; i<lowerLipBottom.getPoints().size(); i++){
            Float px = lowerLipBottom.getPoints().get(i).getX();
            Float py = lowerLipBottom.getPoints().get(i).getY();
            outPath.lineTo(px, py);
        }
        outPath.lineTo(upperLipTop.getPoints().get(0).getX(), upperLipTop.getPoints().get(0).getY());
        outPath.close();


        FirebaseVisionFaceContour upperLipBottom = face.getContour(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM);
        inPath.moveTo(upperLipBottom.getPoints().get(0).getX(), upperLipBottom.getPoints().get(0).getY());
        for(int i=1; i<upperLipBottom.getPoints().size(); i++){
            Float px = upperLipBottom.getPoints().get(i).getX();
            Float py = upperLipBottom.getPoints().get(i).getY();
            inPath.lineTo(px, py);
        }

        FirebaseVisionFaceContour lowerLipTop = face.getContour(FirebaseVisionFaceContour.LOWER_LIP_TOP);
        for(int i=0; i<lowerLipTop.getPoints().size(); i++){
            Float px = lowerLipTop.getPoints().get(i).getX();
            Float py = lowerLipTop.getPoints().get(i).getY();
            inPath.lineTo(px, py);
        }
        inPath.lineTo(upperLipBottom.getPoints().get(0).getX(), upperLipBottom.getPoints().get(0).getY());
        inPath.close();

        //outPath.op(inPath, Path.Op.DIFFERENCE);
        return  outPath;
    }
}
