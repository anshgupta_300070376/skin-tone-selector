package com.example.virtuallipstickmlkit.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;

public class MakeupBeautyUtils {

    public void progress(final Bitmap bitmap, final Handler handler){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(bitmap == null) return;
                Message obtain = Message.obtain();

                Bitmap mutableBitmap = Bitmap.createBitmap(bitmap.getWidth(),bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(mutableBitmap);
                Rect bitmapRect = new Rect(0,0,bitmap.getWidth(),bitmap.getHeight());
                canvas.drawBitmap(bitmap,bitmapRect,bitmapRect,null);
                obtain.obj = mutableBitmap;
                obtain.what = 0;
                handler.sendMessage(obtain);
            }
        }).start();
    }
}
